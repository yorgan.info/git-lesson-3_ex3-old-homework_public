﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_lesson_7_ex6
{
    internal class UserInterfaceEx6
    {

        public int InNumData(string info = "Введіть число: ")
        {

            int operand;
            while (true)
            {
                try
                {
                    Console.Write(info);
                    operand = int.Parse(Console.ReadLine());
                    break;
                }
                catch
                {
                    ShowError();
                }
            }
            return operand;
        }


        public void ShowError(string error = "От халепа напевно якась помилка! Давай ще раз!")
        {

            ConsoleColor color = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(error);
            Console.ForegroundColor = color;

        }

    }
}
