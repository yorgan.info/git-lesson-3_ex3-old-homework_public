﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_lesson_7_ex6
{
    internal class Program
    {
        static void Main(string[] args)
        {

            UserInterfaceEx6 userInterfaceEx6 = new UserInterfaceEx6();
            List<int> list = new List<int>();

            // я вирішив зробити так щоб мона було водити стильки значень скільки потрібно не тільки 3
            bool logic = true;
            while (logic)
            {
                list.Add(userInterfaceEx6.InNumData());
                Console.Clear();
                Console.WriteLine("Хотите прекратить: Y - Да; Щоб продолжити люба Ентер");
                Console.Write("Вибір:  ");
                string openLoop = Console.ReadLine().ToLower();

                if (openLoop == "y")
                {
                    break;
                }
                Console.Clear();
            }

            Calculate(list);


            Console.ReadKey();
        }


        //может я немного перестарався використовуючи List а ле як намене це трохи зручніше 
        static void Calculate(List<int> list)
        {
            double sum = default;
            if (list.Count == 1)
            {
                Console.WriteLine("Тут тільки оне значення:" + list[0]);
            }
            else
            {
                foreach (var item in list)
                {
                    sum += item;
                }

                sum /= list.Count;

                Console.WriteLine($"Середне арифметичне: {Math.Round(sum,2)}");
            }



        }

    }
}
