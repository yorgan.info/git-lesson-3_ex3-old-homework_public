﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_lesson_7_ex3
{
    internal class CurrencyConverter
    {

        public void Converter(in double amountСonvert, in double well, in bool flagConvert = true)
        {

            double sum;
            if (flagConvert)
            {
                sum = amountСonvert * well;
            }
            else
            {
                sum = amountСonvert / well; //перевірка на нуль е в функції приему данних
            }

            Console.Clear();
            Console.WriteLine(new String('-', 50));
            Console.WriteLine((flagConvert) ? "Конвертація ІЗ валюти" : "Конвертація в валюту");
            Console.WriteLine($"Сума для конвертації: {amountСonvert}; Поточний курс: {well}");
            Console.WriteLine($"\nРезультат конвертації {Math.Round(sum,2)}");
            Console.WriteLine(new String('-', 50));
        }


    }
}
