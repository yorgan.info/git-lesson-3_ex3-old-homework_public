﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_lesson_7_ex3
{
    internal class UserInterfaceEx3
    {

        public double InNumData(string info = "Введіть число: ")
        {

            double operand;
            while (true)
            {
                try
                {
                    Console.Write(info);
                    operand = double.Parse(Console.ReadLine());
                    if (operand > 0) //це про всяк выпадок так як ми не можемо конвертувати 0 або відемне число
                    {
                        break;
                    }
                    else
                    {
                        ShowError();
                    }
                }
                catch
                {
                    ShowError();
                }
            }
            return operand;
        }

        public void ShowError(string error = "От халепа напевно якась помилка! Давай ще раз!")
        {

            ConsoleColor color = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(error);
            Console.ForegroundColor = color;

        }

        public bool FlagConvert()
        {
            bool flaglogic = true;

            while (true)
            {
                Console.WriteLine(new String('-', 50));
                Console.WriteLine("Віберіть тип конвертації:");
                Console.WriteLine("Из валюти - 1; В валюту - 2");

                Console.Write("Ваш вибір: ");
                string inFlag = Console.ReadLine();

                Console.WriteLine(new String('-', 50));

                if (inFlag == "1")
                {
                    flaglogic = true;
                    break;

                }
                else if (inFlag == "2")
                {
                    flaglogic = false;
                    break;
                }
                else
                {
                    ShowError();
                    continue;
                }
            }


            return flaglogic;

        }
    }
}
