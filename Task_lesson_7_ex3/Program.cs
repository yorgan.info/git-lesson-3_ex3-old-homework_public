﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Task_lesson_7_ex3
{
    internal class Program
    {
        static void Main(string[] args)
        {


            UserInterfaceEx3 userInterface = new UserInterfaceEx3();
            CurrencyConverter currencyConverter = new CurrencyConverter();

            double amountСonvert = userInterface.InNumData("Введить суму для конвертації: ");
            double well = userInterface.InNumData("Введіть вартість валюти: ");

            bool flagConvert = userInterface.FlagConvert(); //бонусний функціонал)


            //in тут не тещоб порібен (але так як значення не міняется) це е способ показати 
            //що знаю як їх використовувати ref out in.
            //сюди мона було передати зміну sum черз out щоб відразу записати данні і вивести без повернення
            //але в мене данні виводятся отразу в методі
            currencyConverter.Converter(in amountСonvert, in well, in flagConvert); 

            Console.ReadKey();
        }
    }
}
