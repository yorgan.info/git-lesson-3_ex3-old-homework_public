﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_lesson_7_ex4
{
    internal class Program
    {
        static void Main(string[] args)
        {

            NumberCheck numberCheck = new NumberCheck();
     
            int namber;
            while (true)
            {
                try
                {
                    Console.Write("Введить число: ");
                    namber = int.Parse(Console.ReadLine().Replace(" ", ""));
                    break;
                }
                catch
                {
                    ShowError();
                }
            }


            //можливо булj створити і один метод але я врішив зроби кожен метод під свою перевірку

            Console.Clear();
            Console.WriteLine($"Перевірка числа: {namber}");

            numberCheck.PositiveNamberCheck(namber);

            numberCheck.PrimeNumberCheck(namber);

            numberCheck.IsNumDivCheck(namber);

            Console.ReadKey();

        }

        static void ShowError(string error = "От халепа напевно якась помилка! Давай ще раз!")
        {

            ConsoleColor color = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(error);
            Console.ForegroundColor = color;

        }
    }
}
