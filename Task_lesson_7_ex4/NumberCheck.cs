﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_lesson_7_ex4
{
    internal class NumberCheck
    {

        public void PositiveNamberCheck(int number)
        {
            string result;

            if (number >= 0)
            {
                result = "Число є позитивне!";
            }
            else
            {
                result = "Число є негативне!";
            }

            Console.WriteLine(new String('-', 50));
            Console.WriteLine("Перевірка чи число позитивне або негативне:");
            Console.WriteLine(result);
            Console.WriteLine(new String('-', 50));
        }

        public void PrimeNumberCheck(int number)
        {

            bool logic = true;
            for (int i = 2; i <= number / 2; i++)
            {
                if (number % i == 0)
                {
                    logic = false;
                    break;
                }
            }

            string result;
            if (logic && number > 0)
            {
                result = "Число просте";
            }
            else if (number < 0)
            {
                result = "це негативне число, у негативних чисел таке визначення не використовується.";
            }
            else
            {
                result = "Число НЕ просте";
            }

            Console.WriteLine(new String('-', 50));
            Console.WriteLine("Перевірка чи просте число чи ні: ");
            Console.WriteLine(result);
            Console.WriteLine(new String('-', 50));

        }

        public void IsNumDivCheck(int number)
        {
            string dividing = default;
            string notShared = default;


            //вирішив перевірити і вивести на які ділется а на які ні окремо
            int[] array = { 2, 5, 3, 6, 9 };

            foreach (int i in array)
            {
                if (number % i == 0)
                {
                    dividing += i + " ";
                }
                else
                {
                    notShared += i + " ";
                }
            }

            if (dividing == null)
            {
                dividing = "Не на які не ділится";
            }
            else if (notShared == null)
            {
                notShared = "На всі ділится";
            }

            Console.WriteLine(new String('-', 50));
            Console.WriteLine("Перевірка чи ділется число на 2, 5, 3, 6, 9 без залишку:");
            Console.WriteLine("Ділится на: " + dividing);
            Console.WriteLine("Не ділится на: " + notShared);
            Console.WriteLine(new String('-', 50));

        }

    }
}
