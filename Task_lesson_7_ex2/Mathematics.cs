﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_lesson_7_ex2
{
    internal class Mathematics
    {
        UserInterface userInterface = new UserInterface();

        public void Add(double oper1, double oper2)
        {
            Console.Clear();
            Console.WriteLine(new String('-',50));
            Console.WriteLine($"Результат операції: {oper1} + {oper2} буде {oper1 + oper2}");
            Console.WriteLine(new String('-', 50));

        }

        public void Sub(double oper1, double oper2)
        {

            Console.Clear();
            Console.WriteLine(new String('-', 50));
            Console.WriteLine($"Результат операції: {oper1} - {oper2} буде {oper1 - oper2}");
            Console.WriteLine(new String('-', 50));

        }

        public void Mul(double oper1, double oper2)
        {

            Console.Clear();
            Console.WriteLine(new String('-', 50));
            Console.WriteLine($"Результат операції: {oper1} * {oper2} буде {oper1 * oper2}");
            Console.WriteLine(new String('-', 50));

        }

        public void Div(double oper1, double oper2)
        {

            if (oper1 != 0 && oper2 != 0)
            {
                Console.Clear();
                Console.WriteLine(new String('-', 50));
                Console.WriteLine($"Результат операції: {oper1} / {oper2} буде {oper1 / oper2}");
                Console.WriteLine(new String('-', 50));
            }
            else
            {
                userInterface.ShowError("Ну як так можа? навіть малеча знае що на нуль діліти не можна! ДАавай все знову!");
            }

        }

    }
}
