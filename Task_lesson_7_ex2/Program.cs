﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace Task_lesson_7_ex2
{
    internal class Program
    {
        static void Main(string[] args)
        {

            //Я тут вирішив по дрібницях не розмінюватися. Гулять так гулять))

            //тут знахлдятся математичні функції
            Mathematics mathematics = new Mathematics();

            //функції для вводу опрендів ита оператора а також показ помилок;
            UserInterface userInterface = new UserInterface();


            while (true)
            {
                double operand1 = userInterface.InNumData("Введить число 1: ");

                string mathOperator = userInterface.InMathOperator();

                double operand2 = userInterface.InNumData("Введить число 2: ");


                switch (mathOperator)
                {
                    case "+":
                        {
                            mathematics.Add(operand1, operand2);
                            break;
                        }
                    case "-":
                        {
                            mathematics.Sub(operand1, operand2);
                            break;
                        }
                    case "*":
                        {
                            mathematics.Mul(operand1, operand2);
                            break;
                        }
                    case "/":
                        {
                            mathematics.Div(operand1, operand2);
                            break;
                        }
                        default:
                        {
                            userInterface.ShowError("Дивно все повинно працювати(");
                            break;
                        }
                }

                Console.Write("\nНатисни на будь яку кнопку щоб продовжити!");
                Console.ReadKey();
                Console.Clear();
            }


        }
    }
}
