﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_lesson_7_ex6V2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //цего разу я вси функції робив в одному файлі

            //це варіант 6 завдання з використання out для наглядності його вікористання бо в минулих проктах я ні ref ні out не вікористовував

            double sum;

            int namber1 = InNumData("Введіть 1 число: ");
            int namber2 = InNumData("Введіть 2 число: ");
            int namber3 = InNumData("Введіть 3 число: ");


            Calculate(namber1, namber2, namber3, out sum);

            Console.Clear();
            Console.WriteLine(new String('-',50));
            Console.WriteLine($"Середне арифметичне: {Math.Round(sum, 2)}");
            Console.WriteLine(new String('-', 50));

            Console.ReadKey();

        }


        static void Calculate(int namber1, int namber2, int namber3, out double sum)
        {
            sum = (namber1 + namber2 + namber3) / 3;
        }

        static int InNumData(string info = "Введіть число: ")
        {

            int operand;
            while (true)
            {
                try
                {
                    Console.Write(info);
                    operand = int.Parse(Console.ReadLine().Replace(" ",""));
                    if (operand > 0) //це про всяк выпадок так як ми не можемо конвертувати 0 або відемне число
                    {
                        break;
                    }
                    else
                    {
                        ShowError();
                    }
                }
                catch
                {
                    ShowError();
                }
            }
            return operand;
        }


        static void ShowError(string error = "От халепа напевно якась помилка! Давай ще раз!")
        {

            ConsoleColor color = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(error);
            Console.ForegroundColor = color;

        }

    }
}
